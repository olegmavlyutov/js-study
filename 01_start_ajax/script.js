// UI - User Interface

const resultBlock = document.querySelector("#result");
const pageNumberEl = document.querySelector("#page-number");
const countNumberEl = document.querySelector("#count-number");
const clickMeButton = document.querySelector("#click-me");

const getTasksButton = document.querySelector("#get-tasks");

const deleteTaskButton = document.querySelector('#delete-task-button');
const deleteTaskIdInput = document.querySelector("#task-delete-id-input");

const createTaskButton = document.querySelector('#create-task-button');
const createTaskInput = document.querySelector("#create-task-input");
const updateTaskButton = document.querySelector("#update-task-button");


// GET IMAGES
clickMeButton.addEventListener("click", () => {
    const promise = getImages(pageNumberEl.value, countNumberEl.value);
    promise.then(onImagesReceived);
});

// GET TASKS
getTasksButton.addEventListener("click", () => {
    const promise = getTasks();
    promise.then(onTasksReceived);
});

// CREATE TASK
createTaskButton.addEventListener("click", () => {
    const promise = createTask(createTaskInput.value);
    promise.then(alert(`Task ${createTaskInput.value} created`));
    createTaskInput.value = '';
});

// UPDATE TASK
updateTaskButton.addEventListener("click", () => {
    const promise = updateTask(deleteTaskIdInput.value, createTaskInput.value);
    promise.then(alert(`Task ${deleteTaskIdInput.value} updated`));
    createTaskInput.value = '';
});

// DELETE TASK
deleteTaskButton.addEventListener("click", () => {
    const promise = deleteTask(deleteTaskIdInput.value);
    promise.then(alert(`Task ${deleteTaskIdInput.value} deleted`));
    deleteTaskIdInput.value = '';
});

// AJAX CREATE IMAGES IN HTML
let onImagesReceived = (images) => {
    images.forEach(image => {
        const img = document.createElement('img');
        img.src = image.thumbnail;
        document.querySelector("#result").appendChild(img);
    });
}

// AJAX CREATE TASKS IN HTML
let onTasksReceived = (tasks) => {
    const result = document.querySelector('#tasks-result');
    result.innerHTML = '';
    tasks.forEach(task => {
        const li = document.createElement('li');
        li.innerHTML = task.title + '<br>' + `${task.id}`;
        li.id = task.id;
        result.appendChild(li);
    });
}