// DAL - Data Access Layer

function getImages(pageNumber, countNumber) {
    const promise = axios.get(`https://repetitora.net/api/JS/Images?page=${pageNumber}&count=${countNumber}`);
    return promise.then((response) => {
        return response.data;
    });
}

function getTasks() {
    const promise = axios.get(`https://repetitora.net/api/JS/Tasks?widgetId=2428059`);
    return promise.then((response) => {
        return response.data;
    });
}

function createTask(title) {
    const promise = axios.post(`https://repetitora.net/api/JS/Tasks`, {
        widgetId: 2428059,
        title: title
    });
    return promise.then((response) => {
        return response.data;
    });
}

// axios API request
function updateTask(id, title) {
    const promise = axios({
        method: 'put',
        url: `https://repetitora.net/api/JS/Tasks`,
        data: {
            widgetId: 2428059,
            taskId: id,
            title: title
        }
    });
    return promise.then((response) => {
        return response.data;
    });
}

function deleteTask(id) {
    const promise = axios.delete(`https://repetitora.net/api/JS/Tasks?widgetId=2428059&taskId=${id}`);
    return promise.then((response) => {
        return response.data;
    });
}