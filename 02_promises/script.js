let doItAfter = (seconds) => {
    let promise = new Promise((resolve, reject) => {
        console.log(`Promise made. After ${seconds} seconds you'll see it`);
        setTimeout(() => {
            resolve(seconds);
        }, seconds * 1000);
    });
    return promise;
}

doItAfter(Math.random() * 2)
    .then((second) => console.log(`Log after ${second} seconds`));

doItAfter(Math.random() * 2)
    .then((second) => console.log(`Log after ${second} seconds`));

doItAfter(Math.random() * 2)
    .then((second) => console.log(`Log after ${second} seconds`));